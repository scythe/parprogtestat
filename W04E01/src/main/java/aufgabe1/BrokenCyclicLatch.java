package aufgabe1;
import java.util.concurrent.CountDownLatch;


public class BrokenCyclicLatch {
	private static final int NOF_ROUNDS = 10; 
	private static final int NOF_THREADS = 10;
	
	private static CountDownLatch latch = new CountDownLatch(NOF_THREADS);
	
	private static void multiRounds(int threadId) throws InterruptedException {
		for (int round = 0; round < NOF_ROUNDS; round++) {
			latch.countDown();
			latch.await();
			/*
			 * Es ist nicht garantiert, dass der Thread 0 zuerst wieder gestartet wird.
			 * 
			 * Startet ein anderer Thread zuerst, wird auf dem existierenden Latch die 
			 * Methode .countDown() und .await() aufgerufen (nichts passiert!)
			 * und er "verspielt" somit seine Runden bis Thread 0 einen neuen Latch erzeugt.
			 * 
			 * Sobald der erste Thread alle 10 Runden durchlaufen hat, blockieren die restlichen
			 * Threads weil ein Thread f�r den countDown() fehlt.
			 */
			if (threadId == 0) {
				latch = new CountDownLatch(NOF_THREADS); // new latch for new round
			}
			System.out.println("Round " + round + " thread " + threadId);
		}
	}
	
	public static void main(String[] args) {
		for (int i = 0; i < NOF_THREADS; i++) {
			int threadId = i;
			new Thread(() -> {
				try {
					multiRounds(threadId);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}).start();
		}
	}
}
