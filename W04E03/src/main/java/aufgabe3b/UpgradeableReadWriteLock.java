package aufgabe3b;

import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class UpgradeableReadWriteLock {

	ReadWriteLock rwLock = new ReentrantReadWriteLock(false);
	Semaphore upgradeableLock = new Semaphore(1);

	public void readLock() throws InterruptedException {
		rwLock.readLock().lock();
	}

	public void readUnlock() {
		rwLock.readLock().unlock();
	}

	public void upgradeableReadLock() throws InterruptedException {
		rwLock.readLock().lock();
		upgradeableLock.acquire();
	}

	public void upgradeableReadUnlock() {
		upgradeableLock.release();
		rwLock.readLock().unlock();
	}

	public void writeLock() throws InterruptedException {
		rwLock.writeLock().lock();
	}

	public void writeUnlock() {
		rwLock.writeLock().unlock();
	}
}
