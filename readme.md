Dieses Repository enth�lt die Testate von Lukas K�lbener und Michael Gysel
f�r das Modul ParProg an der [HSR](http://www.hsr.ch/).

-----------------------
# Testat 1 - Woche 4

## Aufgabe 1

    :::java
    if (threadId == 0) {
    	latch = new CountDownLatch(NOF_THREADS); // new latch for new round
    }

Es ist nicht garantiert, dass der Thread 0 zuerst wieder gestartet wird.

Startet ein anderer Thread zuerst, wird auf dem existierenden Latch die 
Methode `.countDown()` und `.await()` aufgerufen (nichts passiert!)
und er "verspielt" somit seine Runden bis Thread 0 einen neuen Latch erzeugt.

Sobald der erste Thread alle 10 Runden durchlaufen hat, blockieren die restlichen
Threads weil ein Thread f�r den `countDown()` fehlt.

## Aufgabe 2

### 2.a)

![Betriebsmittelgraph](https://bitbucket.org/scythe/parprogtestat/raw/46a2f207f015307cbe7bd596e2de18298a09f407/W04E02/src/main/resources/aufgabe2a/Betriebsmittelgraph.png)

### 2.b)

Die L�sung funktioniert in dem Sinne, dass sie Deadlocks vermeidet. 
Jedoch muss dazu genau w�hrend des Sleeps der Thread welcher die rechte Gabel 
besitzt aktiv sein und die Gabel freigeben, ohne sie wieder auf zu nehmen. 
Dies kann im Extremfall sehr lange dauern und ist daher suboptimal, bzw. 
kann zu Starvation f�hren.

### 2.c)

Implementation im Folder: `W04E02/src/main/java/aufgabe2c`

## Aufgabe 3

### 3.a)

> Wieso soll der Upgrade-Wunsch bereits beim Read Lock speziell 
> mit upgradeableReadLock() bekannt gegeben werden?

Weil sonst die gelesenen Daten stale sein k�nnen. Nur ein Thread darf jeweils einen
upgradeableReadLock besitzen.

Fehlerszenario:

    Thread A - upgradeableReadLock()
    Thread B - upgradeableReadLock()
    Thread A - liest X
    Thread B - liest X
    
    Thread A - writeLock()
    Thread A - schreibt X
    Thread A - writeUnlock()
    
    Thread B - writeLock() <- wartet auf Thread A
    Thread B - schreibt X <- X ist hier stale!
    Thread B - writeUnlock()
    
    Thread A - upgradeableReadUnlock()
    Thread B - upgradeableReadUnlock()



### 3.b)

Implementation im Folder: `W04E03/src/main/java/aufgabe3b`

### 3.c)

Noch nicht gel�st.

-----------------------
# Testat 2 - Woche 8

## Aufgabe 1

Bei Weak Cache Consistency geht es um die Thread-eigenen Caches, auf welchen die Threads Daten auslesen. Dieser Cache ist nicht immer mit dem Hauptspeicher synchronisiert, wodurch unterschiedliche Speicherzust�nde in verschiedenen Threads auftreten k�nnen. 

Beim Peterson Mutex kann es deshalb vorkommen, dass �nderungen an den Variablen f�r den gegenseitigen Ausschluss nicht sofort vom anderen Thread gesehen werden und der Algorithmus deshalb nicht richtig funktioniert. Ausserdem kann es aufgrund der As-if-serial Semantik zu fehlerhaften Umordnungen kommen. 

Damit der Alogrithmus korrekt abl�uft, muss eine Synchronisation der Caches �ber den Hauptspeicher gemacht werden. Dies wird mittels Volatile Keyword gemacht, welches vor jedem Zugriff eine Synchronisierung ausl�st. Bei .NET reichte diese L�sung nicht aus (volatile ist nur partial fence bei Umordnungen), weshalb wir die turn Variable mit Thread.memoryBarrier() sichern mussten, welche Umordnungen st�rker verhindert wie volatile. 

Frage an L.Bl�ser: Volatile macht auch in .NET eine synchronisierung mit dem Hauptspeicher und das Thread.memoryBarrier() ist in .NET nur n�tig, weil dies im Gegensatz zu volatile unerlaubte Umordnungen verhindert. korrekt?

Implementation im File: `/W08E01/java/src/main/java/aufgabe1/PetersonMutex.java`
Implementation im File: `/W08E01/dotNet/Peterson/PetersonMutex.cs`


## Aufgabe 2

Implementation im File: `/W08E02/src/main/java/aufgabe2/BankAccount.java`

## Aufgabe 3

Implementation im File: `/W08E03/src/main/java/aufgabe3/LockFreeStack.java`

-----------------------
# Testat 3


-----------------------
# Testat 4


-----------------------