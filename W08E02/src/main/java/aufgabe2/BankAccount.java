package aufgabe2;

import java.util.concurrent.atomic.AtomicInteger;

public class BankAccount {
	private AtomicInteger abalance = new AtomicInteger(0);

	public void deposit(int amount) {
		abalance.addAndGet(amount);
	}

	public boolean withdraw(int amount) {
		int currentBalance;
		int newBalance;
		do {
			currentBalance = abalance.get();
			if (amount <= currentBalance) {
				newBalance = currentBalance - amount;
			} else {
				return false;
			}
		} while (!abalance.weakCompareAndSet(currentBalance, newBalance));
		return true;
	}

	public int getBalance() {
		return abalance.get();
	}
}
