package aufgabe3;

import java.util.concurrent.atomic.AtomicReference;

public class LockFreeStack<T> implements Stack<T> {
	AtomicReference<Node<T>> top = new AtomicReference<>();

	public void push(T value) {
		Node<T> newNode = new Node<T>(value);
		Node<T> current;
		do {
			current = top.get();
			newNode.setNext(current);
		} while (!top.compareAndSet(current, newNode));
	}

	public T pop() {
		Node<T> current;
		Node<T> newNode;
		do {
			current = top.get();
			newNode = current.getNext();
		} while (!top.weakCompareAndSet(current, newNode));
		return current.get();
	}

	private class Node<S> {
		S value;
		private Node<S> next;

		public Node(S value) {
			this.value = value;
		}

		S get() {
			return value;
		}

		public void setNext(Node<S> next) {
			this.next = next;
		}

		public Node<S> getNext() {
			return next;
		}

	}
}
